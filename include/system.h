#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include <mem.h>
#include <console.h>
#include <strings.h>
#include <io.h>
#include <gdt.h>
#include <idt.h>
#include <isr.h>
#include <irq.h>
#include <pit.h>
#include <basic.h>
#include <kbd.h>
#include <paging.h>
#include <serial.h>
#include <heap.h>
#include <acpi/acpi.h>
#include <multiboot.h>
#include <fs/initrd.h>
#include <frame.h>

#endif
