#ifndef __STRINGS_H__
#define __STRINGS_H__

#include <types.h>
#include <console.h>

void *memcpy(void *dest,
             const void *src,
             int count);

void *memset(void *dest,
             unsigned char val,
             int count);

void *memcpy2(void *dest,
              const void *src,
              int length,
              int count);

u32int memcmp(void *m1, void *m2, u32int l);

int itoa(void *buf, int base, int d);
void dump(void *, u32int size);

#endif
