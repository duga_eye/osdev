#ifndef __ACPI_H__
#define __ACPI_H__

#include <acpi/types.h>
#include <strings.h>
#include <console.h>

#include <frame.h>
#include <paging.h>

u32int find_RDSP();
void init_acpi ();
void AcpiEnable();
void AcpiPowerOff();
u32int rsdp_verification(RSDPv1_t *rdsp);
u32int header_verification(SDT_header_t *header);
u32int find_header(u32int rsdt, char signature[4]);
#endif
