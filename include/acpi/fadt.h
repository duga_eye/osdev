#ifndef __ACPI_FADT__
#define __ACPI_FADT__

#include <types.h>

struct ACPISDTHeader {
  char Signature[4];
  u32int Length;
  u8int Revision;
  u8int Checksum;
  char OEMID[6];
  char OEMTableID[8];
  u32int OEMRevision;
  u32int CreatorID;
  u32int CreatorRevision;
};

typedef struct
{
  u8int AddressSpace;
  u8int BitWidth;
  u8int BitOffset;
  u8int AccessSize;
  u64int Address;
} GenericAddressStructure;

struct FADT
{
    struct   ACPISDTHeader h;
    u32int FirmwareCtrl;
    u32int Dsdt;
 
    // field used in ACPI 1.0; no longer in use, for compatibility only
    u8int  Reserved;
 
    u8int  PreferredPowerManagementProfile;
    u16int SCI_Interrupt;
    u32int SMI_CommandPort;
    u8int  AcpiEnable;
    u8int  AcpiDisable;
    u8int  S4BIOS_REQ;
    u8int  PSTATE_Control;
    u32int PM1aEventBlock;
    u32int PM1bEventBlock;
    u32int PM1aControlBlock;
    u32int PM1bControlBlock;
    u32int PM2ControlBlock;
    u32int PMTimerBlock;
    u32int GPE0Block;
    u32int GPE1Block;
    u8int  PM1EventLength;
    u8int  PM1ControlLength;
    u8int  PM2ControlLength;
    u8int  PMTimerLength;
    u8int  GPE0Length;
    u8int  GPE1Length;
    u8int  GPE1Base;
    u8int  CStateControl;
    u16int WorstC2Latency;
    u16int WorstC3Latency;
    u16int FlushSize;
    u16int FlushStride;
    u8int  DutyOffset;
    u8int  DutyWidth;
    u8int  DayAlarm;
    u8int  MonthAlarm;
    u8int  Century;
 
    // reserved in ACPI 1.0; used since ACPI 2.0+
    u16int BootArchitectureFlags;
 
    u8int  Reserved2;
    u32int Flags;
 
    // 12 byte structure; see below for details
    GenericAddressStructure ResetReg;
 
    u8int  ResetValue;
    u8int  Reserved3[3];
 
    // 64bit pointers - Available on ACPI 2.0+
    u64int                X_FirmwareControl;
    u64int                X_Dsdt;
 
    GenericAddressStructure X_PM1aEventBlock;
    GenericAddressStructure X_PM1bEventBlock;
    GenericAddressStructure X_PM1aControlBlock;
    GenericAddressStructure X_PM1bControlBlock;
    GenericAddressStructure X_PM2ControlBlock;
    GenericAddressStructure X_PMTimerBlock;
    GenericAddressStructure X_GPE0Block;
    GenericAddressStructure X_GPE1Block;
};

#endif
