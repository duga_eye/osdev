#ifndef __ACPI_TYPES_H__
#define __ACPI_TYPES_H__

#include <types.h>

struct RSDPv1 {
   char signature[8];
   u8int checksum;
   char OEMID[6];
   u8int revision;
   u32int rsdt_address;
} __attribute__ ((packed));

typedef struct RSDPv1 RSDPv1_t;

struct RSDPv2 {
   char signature[8];
   u8int checksum;
   char OEMID[6];
   u8int revision;
   u32int rsdt_address;

   u32int length;
   u64int xsdt_address;
   u8int extended_checksum;
   u8int reserved[3];

} __attribute__ ((packed));

typedef struct RSDPv2 RSDPv2_t;

typedef struct SDT_header {
  char Signature[4];
  u32int Length;
  u8int Revision;
  u8int Checksum;
  char OEMID[6];
  char OEMTableID[8];
  u32int OEMRevision;
  u32int CreatorID;
  u32int CreatorRevision;
} __attribute__ ((packed)) SDT_header_t;

#endif
