#ifndef __GENERIC_H__
#define __GENERIC_H__

#include <console.h>

#define UNUSED(x) (void)(x)
#define NULL 0

#define PANIC(x) log("[%s(%s):%d]: ASSERTION '%s' FAILED", __FILE__, __FUNCTION__, __LINE__, x); \
        printf("[%s(%s):%d]: ASSERTION '%s' FAILED", __FILE__, __FUNCTION__, __LINE__, x);  for(;;) hlt();
#define ASSERT(x) do { if (!(x)) { PANIC("ASSERTION FAILED"); } } while(0)
#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))
#define ALIGN_DOWN(x) (x/0x1000)*0x1000



#endif
