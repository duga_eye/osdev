#ifndef __PAGE_H__
#define __PAGE_H__

#include <types.h>

typedef struct page {
    u32int present  : 1;  // strona jest obecna w pamięci
    u32int rw       : 1;  // odczyt / zapis jeśli 1, inaczej tylko odczyt
    u32int user     : 1;  // user-space jeśli 1, inaczej kernel-space
    u32int accessed : 1;  // flaga ustawiana przez cpu - czy była użytkowana
    u32int dirty    : 1;  // flaga ustawiana przez cpu - czy była zapisywana
    u32int unused   : 7;  // nieużywane
    u32int frame    : 20; // adres framki (przesuniętego o 12 bitów)
} page_t;

typedef struct page_table {
    page_t pages[1024];
} page_table_t;

typedef struct page_directory {
    /**
     * Tablica wskaźników do tablic stron.
     */
    page_table_t *table[1024];
    /**
     * Tablica wskaźników do tablic stron, ale fizycznych lokalizacji
     * w celu załadowania ich do rejestru CR3.
     */
    u32int tablesPhysical[1024];
    /**
     * Fizyczny adres tabliy tablesPhysical.
     */
    u32int physicalAddr;
} page_directory_t;

#endif
