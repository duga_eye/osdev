#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <strings.h>

#define VGA_ADDRESS 0xB8000
#define VGA_WIDTH 80
#define VGA_HEIGHT 25
#define COLOURS 0x0F
#define PRINTABLE(c) (c >= ' ')

#include <serial.h>

extern volatile unsigned short int *scr;
extern int curx,cury;
extern unsigned short int EmptySpace;

int scroll();
int putchar(unsigned char c);
int puts(unsigned char *str);
int vga_clear();
int vga_init();
int snprintf(char *buffer, int length, char *format,...);
int printf(const char *format, ...);
int log(const char *format,...);

#endif
