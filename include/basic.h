#ifndef __BASIC_H__
#define __BASIC_H__

#include <types.h>

extern void sti();
extern void cli();
extern void io_wait();
extern u32int kernel_start();
extern u32int kernel_end();
extern u32int kernel_full_end();
extern void hlt();


#endif
