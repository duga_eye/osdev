#ifndef __ISR_H__
#define __ISR_H__

#include <console.h>
#include <idt.h>
#include <serial.h>

struct regs {
    unsigned int gs, fs, es, ds;
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
    unsigned int int_no, err_code;
    unsigned int eip, cs, eflags, useresp, ss;
};

typedef struct regs res_t;


int isrs_install();

#endif
