#ifndef __HEAP_H__
#define __HEAP_H__

#include <generic.h>
#include <types.h>
#include <frame.h>
#include <paging.h>

#define HEAP_MARKER 0xdeadbeaf
#define MAX_BLOCKS 0x10000
#define ALIGNMENT_SIZE 0x1000
#define ALIGN_UP(x) (((x - 1) / ALIGNMENT_SIZE + 1)*ALIGNMENT_SIZE)

typedef struct block_header {
    u32int marker1;
    u32int start;
    u32int size;         // rozmiar z headerem i footerem
    u32int id;
    unsigned char aligned;
    unsigned char kernel;
    unsigned char rw;   // rw - 1, r - 0
    unsigned char block;   // 1 - block, 0 - hole
    u32int marker2;
} block_header_t;

typedef struct block_footer {
    u32int marker1;
    u32int header;
    u32int marker2;
} block_footer_t;

typedef struct heap {
    u32int marker1;
    block_header_t *blocks[MAX_BLOCKS];
    u32int max_id;
    u32int min_address;
    u32int max_address;
    u32int limit_max;
    u32int marker2;
} heap_t;

extern heap_t kernel_heap;

void *kmalloc(u32int size);
void kfree(void *addr);

void *kmalloc_a(u32int size);

void init_heap(heap_t *heap, u32int min_address, u32int max_address,
        u32int limit_max, u32int kernel, u32int rw);

void destroy_heap(heap_t *heap);
int sanity_check(heap_t *heap);
void span_items(heap_t *heap);

void *kalloc_int(u32int size, u32int aligned, u32int kernel, u32int rw,
        heap_t *heap);

void kfree_int(void *addr, heap_t *heap);
void insert_block_array(heap_t *heap, block_header_t *header, u32int max_id);
void dump_heap(heap_t *heap);

#endif
