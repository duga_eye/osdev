#ifndef __PAGING_H__
#define __PAGING_H__


#include <mem.h>
#include <isr.h>
#include <types.h>
#include <isr.h>
#include <heap.h>
#include <generic.h>
#include <strings.h>
#include <page.h>
#include <frame.h>
#include <irq.h>

u32int c_log_phys(u32int logical);
void initialise_paging2();
void switch_page_directory(page_directory_t *new);
page_t *get_page(u32int address, int make, page_directory_t *dir);
int page_fault(struct regs *r);

extern page_directory_t *current_directory;
extern page_directory_t *kernel_directory;

#endif
