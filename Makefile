CXX=clang
LINK=i686-pc-linux-gnu-gcc
SOURCEDIR = src
OUTPUTDIR = bin
TESTDIR = test
CFLAGS := -O0 -g  -Iinclude -std=gnu99 -nostdlib -nostdinc -fno-builtin \
	-ffreestanding -Wall
ASMFLAGS = -g -felf
LDFLAGS = -O0 -ffreestanding -nostdlib -g -T linker.ld
SOURCES_C=$(shell find $(SOURCEDIR) -name \*.c)
SOURCES_ASM=$(shell find $(SOURCEDIR) -name \*.s)
BINARIES_NON=$(SOURCES_C:$(SOURCEDIR)%.c=$(OUTPUTDIR)%.c.o) \
	$(SOURCES_ASM:$(SOURCEDIR)%.s=$(OUTPUTDIR)%.s.o)
FILTER=$(OUTPUTDIR)/arch/boot/boot.s.o
BINARIES=$(filter-out $(FILTER), $(BINARIES_NON))
FILTER_TEST=$(OUTPUTDIR)/arch/basic/kernel.c.o
BINARIES_TEST=$(filter-out $(FILTER_TEST), $(BINARIES_NON))
TESTS2 = $(wildcard $(TESTDIR)/test-*.c)
TESTS = $(TESTS2:$(TESTDIR)/%.c=%)

all: prepare compile

compile: $(BINARIES_NON)
	@echo "Linking kernel..."
	@$(LINK)  $(LDFLAGS) -o bin/myos.bin \
		$(BINARIES)
	@objcopy  $(OUTPUTDIR)/myos.bin $(OUTPUTDIR)/myos.sym

iso: all
	@mkdir -p isodir
	@mkdir -p isodir/boot
	@cp $(OUTPUTDIR)/myos.bin isodir/boot/myos.bin
	@cp $(OUTPUTDIR)/myos.bin isodir/efi.img
	@mkdir -p isodir/boot/grub
	@cp iso/grub.cfg isodir/boot/grub/grub.cfg
	@grub-mkrescue -o $(OUTPUTDIR)/myos.iso isodir > /dev/null 2>&1

$(OUTPUTDIR)/%.s.o: $(SOURCEDIR)/%.s
	@echo "ASM $<"
	@mkdir -p $(shell dirname $@)
	@nasm $(ASMFLAGS)  $< -o $@

$(OUTPUTDIR)/%.c.o: $(SOURCEDIR)/%.c
	@echo "CC $<"
	@mkdir -p $(shell dirname $@)
	@$(CXX) $(CFLAGS) -c $< -o $@

$(TESTDIR)/%.c.o: $(TESTDIR)/%.c
	@echo "CC $<"
	@mkdir -p $(shell dirname $@)
	@$(CXX) $(CFLAGS) -c $< -o $@


test-%: compile-test-%
	@echo "running qemu..."
	- @timeout 15 qemu-system-i386 -serial file:log.txt  -kernel bin/myos.bin
	- @grep "ALL TESTS PASSED" log.txt

compile-test-%: $(BINARIES_TEST) $(TESTDIR)/test-%.c.o
	@echo "Testing $@"
	@echo $(BINARIES_TEST)
	@echo "Linking kernel..."
	@$(CXX) -O0 -ffreestanding -nostdlib -g -T linker.ld -o bin/myos.bin \
		$(filter-out $(OUTPUTDIR)/arch/boot/boot.s.o, $^)

test: $(TESTS)

compile-test: $(addprefix compile-, $(TESTS))

.PHONY: prepare clean qemu test compile-test

prepare:
	@echo "Preparing output directory..."
	@mkdir -p bin

clean:
	@echo "Cleaning output directory..."
	@rm -rf bin isodir
	@find test -name *.o -delete

qemu:
	@echo "running qemu..."
	@qemu-system-i386 -initrd tools/generator/initrd.img -d cpu_reset -serial stdio -kernel bin/myos.bin 2>&1 | tee log.txt
