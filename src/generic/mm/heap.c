#include <heap.h>

#define STRUCTURES_SIZE (sizeof(block_header_t)+sizeof(block_footer_t))

heap_t kernel_heap;

void *kmalloc(u32int size) {
    return kalloc_int(size, 0, 1, 1, &kernel_heap);
}
void kfree(void *addr) {
    kfree_int(addr, &kernel_heap);
}
void *kmalloc_a(u32int size) {
    return kalloc_int(size, 1, 1, 1, &kernel_heap);
}


void init_heap(heap_t *heap, u32int min_address, u32int max_address,
        u32int limit_max, u32int kernel, u32int rw) {
    memset((unsigned char *)heap, 0, sizeof(heap_t));
    heap->min_address = min_address;
    heap->max_address = max_address;
    heap->marker1 = HEAP_MARKER;
    heap->marker2 = HEAP_MARKER;
    heap->max_id = 1;
    heap->blocks[0] = (block_header_t *)min_address;
    heap->limit_max = limit_max;

    // tworzenie początkowego 1. bloku
    block_header_t *header = (block_header_t *)min_address;
    block_footer_t *footer = (block_footer_t *)
        (max_address - sizeof(block_footer_t));

    for(u32int addr = ALIGN_DOWN(min_address); addr <= max_address;
            addr += 0x1000) {
        alloc_frame(get_page(addr, 1, current_directory),1,1);
    }

    header->marker1 = HEAP_MARKER;
    header->marker2 = HEAP_MARKER;
    header->start = min_address;
    header->size = max_address - min_address;
    header->id = 0;
    header->aligned = 0;
    header->block = 0;
    header->kernel = kernel;
    header->rw = rw;

    footer->marker1 = HEAP_MARKER;
    footer->marker2 = HEAP_MARKER;
    footer->header = min_address;

    ASSERT(sanity_check(heap));

}

void destroy_heap(heap_t *heap) {
    ASSERT(heap->marker1);
    ASSERT(heap->marker2);

    for(u32int addr = ALIGN_DOWN(heap->min_address); addr <= heap->max_address;
            addr += 0x1000) {
        if (addr >= kernel_full_end()) {
            free_frame(get_page(addr, 1, current_directory));
        }
    }

    memset((unsigned char *)heap, 0, sizeof(heap_t));

}

int sanity_check(heap_t *heap) {

    for (u32int i = 0; i < heap->max_id; i++) {
        block_header_t *header = heap->blocks[i];
        block_footer_t *footer = (block_footer_t *)
            (heap->blocks[i]->start + heap->blocks[i]->size
                - sizeof(block_footer_t));
        ASSERT(header->marker1 == HEAP_MARKER);
        ASSERT(header->marker2 == HEAP_MARKER);
        ASSERT(footer->marker1 == HEAP_MARKER);
        ASSERT(footer->marker2 == HEAP_MARKER);
    }

    return 1;
}

int find_smallest_gap(u32int  size, u32int aligned, heap_t *heap) {
    int smallest_id = -1;
    if (aligned) {
        // musimy wyrównać region (rozmiar)
        size = ((size - 1) / ALIGNMENT_SIZE + 1) * ALIGNMENT_SIZE;
    }

    u32int needed_size = size + sizeof(block_header_t)
        + sizeof(block_footer_t);

    for (u32int i = 0; i < heap->max_id; i++) {
        if (!heap->blocks[i]->block) {
            // mamy "dziurę"
            u32int gap_start = heap->blocks[i]->start;
            u32int block_start = gap_start;
            u32int gap_size = heap->blocks[i]->size;

            if (aligned) {
                // zdefiniowano wyrównywanie, zaokrąglam do góry adres
                block_start = ((gap_start - 1) / ALIGNMENT_SIZE + 1)
                    * ALIGNMENT_SIZE - sizeof(block_header_t);
            }

            // uwzględniamy rozmiar wewnętrznych struktur
            if (block_start + needed_size > gap_start + gap_size) {
                continue; // za mała przestrzeń
            }

            smallest_id = i;
            break;
            // spełnia wymagania, najmniejsza bo posortowane
            // wg. rozmiaru bloku
        }
    }

    return smallest_id;
}

// FIXME co zrobić jeśli wolnego  miejsca za mało by nawet 
// struktury tam wcisnąć?
//
// 1. Pomysł - przygarnąć na koniec jakiegoś bloku zarezerwowanego
// dać mu więcej niż potrzebuje, nie będzie się skarżył

u32int insert_block(u32int size, u32int aligned, u32int kernel, u32int rw,
        heap_t *heap, u32int id) {
    u32int block_size = size;
    if (aligned) {
        block_size = ALIGN_UP(block_size);
    }
    block_size += sizeof(block_header_t) + sizeof(block_footer_t);
    u32int gap_start = heap->blocks[id]->start;
    u32int gap_end = gap_start + heap->blocks[id]->size;
    u32int block_start = gap_start;
    if (aligned) {
        block_start = ALIGN_UP(block_start+sizeof(block_header_t)) 
            - sizeof(block_header_t);
    }
    u32int block_end = block_start + block_size;
    u32int gap_before_size = block_start - gap_start;
    u32int gap_after_size = gap_end - block_end;

    block_header_t *left_header = (block_header_t *)gap_start;
    block_header_t *header = (block_header_t *)block_start;
    block_header_t *right_header = (block_header_t *)block_end;
    block_header_t c_left = *left_header;
    block_header_t c = *header;
    block_header_t c_right = *right_header;
    block_footer_t *left_footer = (block_footer_t *)
        (block_start - sizeof(block_footer_t));
    block_footer_t *footer = (block_footer_t *)
        (block_end - sizeof(block_footer_t));
    block_footer_t *right_footer = (block_footer_t *)
        (gap_end - sizeof(block_footer_t));

    int blocks_offset = 0;
    // jeśli cały zaalokowany block wypełnia dziurę w całości dziurę

    heap->blocks[id] = NULL; // usuwamy blok z rejestru

    if (gap_before_size >= STRUCTURES_SIZE) {
        blocks_offset++;
    } else if (gap_before_size > 0) {
        // TODO trzeba znaleźć poprzednika
        // a co jeśli go nie ma np. jest na początku

        block_footer_t *preceeder_footer
            = (block_footer_t *)((u32int)left_header - sizeof(block_footer_t));

        ASSERT(preceeder_footer->marker1 == HEAP_MARKER);
        ASSERT(preceeder_footer->marker2 == HEAP_MARKER);

        block_header_t *preceeder_header
            = (block_header_t *)(preceeder_footer->header);

        ASSERT(preceeder_header->marker1 == HEAP_MARKER);
        ASSERT(preceeder_header->marker2 == HEAP_MARKER);

        preceeder_header->size += gap_before_size;

        block_footer_t copy = *preceeder_footer;
        preceeder_footer = (block_footer_t *)
            ((u32int)preceeder_header->start + preceeder_header->size
                - sizeof(block_footer_t));

        *preceeder_footer = copy;

    }

    heap->blocks[id] = NULL; // czyścimy miejsce, w którym już leżał

    if (gap_after_size >= STRUCTURES_SIZE) {
        blocks_offset++;
    } else if (gap_after_size > 0) {
        // TODO
        block_size += gap_after_size;
        footer = (block_footer_t *)
            (block_start + block_size - sizeof(block_footer_t));
        // poprzednik przyjmie więcej niż chciał bez problemu
    }

    // FIXME a co jeśli jakaś wolna przestrzeń jest za mała by
    // nawet wcisnąć tam struktury bloku?
    // teoretycznie można spróbować dokleić na koniec poprzednika
    // ale problem pojawia się w przypadku 1. elementu
    // teoretycznie można zapisać sobie np. w heapie, ale to dziwne
    // rozwiązanie.
    



    /**
     * Usunięte stare deskryptory z tablicy
     */

    span_items(heap);

    if (blocks_offset >= 2) {
        *left_header = c_left;
    }

    *header = c;

    if (blocks_offset >= 1) {
        *right_header = c_right;
    }

    if (blocks_offset >= 2) {
        left_header->size = gap_before_size;
        left_header->block = 0;

        left_footer->marker1 = HEAP_MARKER;
        left_footer->marker2 = HEAP_MARKER;
        left_footer->header = left_header->start;

    }

    if (blocks_offset >= 1) {
        right_header->start = block_end;
        right_header->size = gap_after_size;
        right_header->marker1 = HEAP_MARKER;
        right_header->marker2 = HEAP_MARKER;
        right_header->block = 0;

        right_footer->marker1 = HEAP_MARKER;
        right_footer->marker2 = HEAP_MARKER;
        right_footer->header = right_header->start;

    }

    header->start = block_start;
    header->size = block_size;
    header->marker1 = HEAP_MARKER;
    header->marker2 = HEAP_MARKER;
    header->aligned = aligned;
    header->kernel = kernel;
    header->rw = rw;
    header->block = 1;

    footer->marker1 = HEAP_MARKER;
    footer->marker2 = HEAP_MARKER;
    footer->header = header->start;

    if (blocks_offset >= 2) {
        insert_block_array(heap, left_header, heap->max_id);
    }
    insert_block_array(heap, header, heap->max_id);
    if (blocks_offset >= 1) {
        insert_block_array(heap, right_header, heap->max_id);
    }

    return block_start + sizeof(block_header_t);
}

void *kalloc_int(u32int size, u32int aligned, u32int kernel, u32int rw,
        heap_t *heap) {

    // FIXME owszem wypełniamy struktury block_header_t
    // ale nie naprawiamy wskaźników do niego prowadzących
    // w "blocks"

    if (aligned) {
        // jeśli potrzebujemy wyrównać, wyrównujemy rozmiar
        size = ALIGN_UP(size);
    }

    int smallest_id = find_smallest_gap(size, aligned, heap);
    int needed_size = sizeof(block_header_t) + sizeof(block_footer_t)
                        + size;

    void *ret = NULL;

    if (smallest_id >= 0) {
        // znaleźliśmy wolną przestrzeń w dotychczasowej
        // przesuwamy o 1 wskaźniki
        ret = (void *)insert_block(size,aligned,kernel,rw,heap,smallest_id);
    } else {
        // rozszerzamy na prawo
        u32int block_start = heap->max_address;
        if (aligned) {
            // wyrównujemy adres do jednostki
            block_start = ALIGN_UP(block_start);
        }

        u32int max_address = block_start + needed_size;
        if (max_address >= heap->limit_max) {
            printf("New failed max address: 0x%x\n",
                    max_address);
            PANIC("Max limit of heap was exceeded");
            return NULL;
        }

        for (u32int addr = ALIGN_UP(heap->max_address);
                addr < max_address; addr += 0x1000) {
            alloc_frame(get_page(addr,1, current_directory), 1,rw);
        }

        block_header_t *new_header = (block_header_t *)heap->max_address;
        heap->max_address = max_address;

        insert_block_array(heap, new_header, heap->max_id);
        // wstawiliśmy nowy blok w odpowiednie miejsce
        // nie musieliśmy spanować, bo nie robiliśmy dziur (tylko
        // dodawaliśmy na koniec)

        new_header->marker1 = HEAP_MARKER;
        new_header->start = block_start;
        new_header->size = needed_size;
        new_header->aligned = aligned;
        new_header->kernel = kernel;
        new_header->rw = rw;
        new_header->block = 1;
        new_header->marker2 =  HEAP_MARKER;
        block_footer_t *new_footer = (block_footer_t *)
            (new_header->start + new_header->size - sizeof(block_footer_t));
        new_footer->marker1 = HEAP_MARKER;
        new_footer->marker2 = HEAP_MARKER;
        new_footer->header = new_header->start;
        ret = (void *)(block_start + sizeof(block_header_t));
    }

    ASSERT(sanity_check(heap));
    return ret;
}

// TODO sprawdź, przetestuj funkcję kfree_int, ale wygląda ok.

void kfree_int(void *addr, heap_t *heap) {
    u32int a = (u32int)addr;
    block_header_t *header = (block_header_t *)(a - sizeof(block_header_t));
    block_footer_t *footer = (block_footer_t *)
        ((u32int)header + header->size - sizeof(block_footer_t));

    ASSERT(header->marker1 == HEAP_MARKER);
    ASSERT(header->marker2 == HEAP_MARKER);

    ASSERT(footer->marker1 == HEAP_MARKER);
    ASSERT(footer->marker2 == HEAP_MARKER);

    ASSERT(header->block);

    u32int block_end = (u32int)header + header->size;
    if (block_end >= heap->max_address) {
        // blok nasz jest ostatnim na prawo
        // trzeba zwęzić

        for (u32int addr = ALIGN_UP(header->start); addr < heap->max_address;
                addr += 0x1000) {
            free_frame(get_page(addr, 0, current_directory));
        }

        heap->max_address = header->start;

        heap->blocks[header->id] = NULL; // usuwamy jego adres z bloków
        span_items(heap); // usuwamy dziurę w tablicy
    } else {
        // blok nasz jest gdzieś w środku
        block_footer_t *left_footer =
            (block_footer_t *)((u32int)header - sizeof(block_footer_t));
        block_header_t *left_header = (block_header_t *)
            (left_footer->header);

        block_header_t *right_header = (block_header_t *)
            ((u32int)header + header->size);
        block_footer_t *right_footer =
            (block_footer_t *)
            ((u32int)right_header + right_header->size-sizeof(block_footer_t));

        int rA = 0;

        if ((u32int)left_footer >= heap->min_address) {
            rA = 1;
            ASSERT(left_header->marker1 == HEAP_MARKER);
            ASSERT(left_header->marker2 == HEAP_MARKER);

            ASSERT(left_footer->marker1 == HEAP_MARKER);
            ASSERT(left_footer->marker2 == HEAP_MARKER);
        }

        int rB = 0;

        if ((u32int)right_header <= heap->max_address) {
            rB = 1;
            ASSERT(right_header->marker1 == HEAP_MARKER);
            ASSERT(right_header->marker2 == HEAP_MARKER);

            ASSERT(right_footer->marker1 == HEAP_MARKER);
            ASSERT(right_footer->marker2 == HEAP_MARKER);
        }

        header->block = 0;
        u32int start = header->start;
        u32int end = start + header->size;

        if (rA && !(left_header->block)) {
            start = left_header->start;
            heap->blocks[left_header->id] = NULL;
            // usuwamy nadmiarowy gap
        }

        if (rB && !(right_header->block)) {
            end = right_header->start +right_header->size;
            heap->blocks[right_header->id] = NULL;
            // usuwamy nadmiarowy gap
        }

        heap->blocks[header->id] = NULL; // siebie też usuwamy

        u32int size = end-start;
        block_header_t *new_header = (block_header_t *)start;
        block_footer_t *new_footer = (block_footer_t *)
            (end - sizeof(block_footer_t));

        new_footer->header = start;
        new_header->size = size;
        new_header->block = 0;
        span_items(heap);

        insert_block_array(heap,new_header, heap->max_id);

    }

    ASSERT(sanity_check(heap));

}

void span_items(heap_t *heap) {
    if (heap == NULL) {
        PANIC("Heap cannot be NULL");
        return;
    }

    ASSERT(heap->marker1 == HEAP_MARKER);
    ASSERT(heap->marker2 == HEAP_MARKER);

    int idx = 0;

    for (u32int i = 0; i < heap->max_id; i++) {
        if (heap->blocks[i]) {
            heap->blocks[idx] = heap->blocks[i];
            heap->blocks[idx]->id = idx;
            idx++;
        }
    }

    heap->max_id = idx;

}

int find_idx_to_insert(heap_t *heap, block_header_t *header, u32int max_id) {
    for (u32int i = 0; i < max_id; i++) {
        if (!heap->blocks[i]) {
            continue;
        }

        if (heap->blocks[i]->size >= header->size) {
            return i;
        }

    }

    return max_id;

}

void insert_block_array(heap_t *heap, block_header_t *header, u32int max_id) {
    if (heap->max_id + 1 >= MAX_BLOCKS) {
        PANIC("NO FREE MEMORY DESCRIPTOR SLOTS");
        return;
    }

    u32int idx = find_idx_to_insert(heap,header,max_id);
    for (u32int i = max_id; i >= idx + 1; i--) {
        heap->blocks[i] = heap->blocks[i-1];
        heap->blocks[i]->id = i;
    }

    heap->blocks[idx] = header;
    heap->blocks[idx]->id = idx;
    heap->max_id++; // dodaliśmy nowy element
}

void dump_heap(heap_t *heap) {
    block_header_t *header = (block_header_t *)heap->min_address;

    printf("----------- START OF MEMORY MAP-----------\n");
    log("----------- START OF MEMORY MAP-----------\r\n");
    printf("Descriptor table: [");
    for (u32int i = 0; i < heap->max_id-1; i++) {
        printf("0x%x(%d), ", (u32int)heap->blocks[i], heap->blocks[i]->size);
    }
    printf("0x%x(%d)]\n", heap->blocks[heap->max_id-1],
            heap->blocks[heap->max_id-1]->size);
    printf("Min address: 0x%x, max address: 0x%x, limit max: 0x%x\n",
            heap->min_address,
            heap->max_address,
            heap->limit_max);

    while ((u32int)header < heap->max_address) {
        ASSERT(header->marker1 == HEAP_MARKER);
        ASSERT(header->marker2 == HEAP_MARKER);

        printf("[0x%x(0x%x)-0x%x(0x%x)] - %d(%d) %s [%c%c%c]\n",
                header->start,
                header->start+sizeof(block_header_t),
                header->start + header->size,
                header->start + header->size - sizeof(block_footer_t),
                header->size - sizeof(block_header_t) - sizeof(block_footer_t),
                header->id,
                header->block ? "BLOCK" : "FREE",
                header->kernel ? 'k' : ' ',
                header->rw ? 'w' : 'r',
                header->aligned ? 'a': ' ');

        log("[0x%x(0x%x)-0x%x(0x%x)] - %d(%d) %s [%c%c%c]\r\n",
                header->start,
                header->start+sizeof(block_header_t),
                header->start + header->size,
                header->start + header->size - sizeof(block_footer_t),
                header->size - sizeof(block_header_t) - sizeof(block_footer_t),
                header->id,
                header->block ? "BLOCK" : "FREE",
                header->kernel ? 'k' : ' ',
                header->rw ? 'w' : 'r',
                header->aligned ? 'a': ' ');


        header = (block_header_t *)((u32int)header + header->size);

    }

    printf("----------- END OF MEMORY MAP-------------\n");
    log("----------- END OF MEMORY MAP-------------\r\n");

}
