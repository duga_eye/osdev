ASM equ 1
MULTIBOOT_HEADER_MAGIC equ 0x1BADB002
MULTIBOOT_HEADER_FLAGS equ 0x00000003
STACK_SIZE equ 0x4000

extern cmain
global _start
global stack
global stack_end

section .multiboot
multiboot_header:
    align 4
    dd MULTIBOOT_HEADER_MAGIC
    dd MULTIBOOT_HEADER_FLAGS
    dd -(MULTIBOOT_HEADER_FLAGS + MULTIBOOT_HEADER_MAGIC)

section .text
_start:
    align 4
    jmp multiboot_entry

multiboot_entry:
    mov esp, stack + STACK_SIZE
    push ebx
    push eax
    call cmain

    ; tu nigdy nie powinno dojść
    add esp, 8

loop:
    hlt
    jmp loop

section .bss

stack:
    resb STACK_SIZE
stack_end:
