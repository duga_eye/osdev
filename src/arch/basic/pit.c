#include <pit.h>

#define MAX_HERTZ 1193180
#define HZ 18
#define quant (MAX_HERTZ / HZ)
#define quant_time ((double) quant / (double)(MAX_HERTZ))
int timer_ticks = 0;
double left_time = 0.0;

int timer_phase(int hz) {

    int divisor = MAX_HERTZ / hz;
    outb(0x43, 0x36);
    outb(0x40, divisor & 0xFF);
    outb(0x40, divisor >> 8);

    return 0;
}

int timer_handler(struct regs *r) {
    UNUSED(r);
    timer_ticks++;
    left_time += quant_time;

    /*
    if (timer_ticks % HZ == 0) {
        printf((char *)"One seconds has passed: %d\n",
                (unsigned int)left_time);
    }
    */
    return 0;
}

int timer_install() {
    irq_install_handler(0, timer_handler);
    return 0;
}

int sleep(double sec) {
    double start = left_time;
    while (left_time - start < sec);
    return 0;
}

