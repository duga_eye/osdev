#include <console.h>
#define COLOUR (COLOURS << 8)

volatile unsigned short int *scr;
int curx = 0, cury = 0;
unsigned short int EmptySpace = COLOUR | 0x20;


int scroll() {
    int distance = cury - VGA_HEIGHT + 1;

    if (distance <= 0) {
        return 0;
    }

    unsigned char *newStart = ((unsigned char *)scr)
        + distance * VGA_WIDTH * 2;
    int bytesToCopy = (VGA_HEIGHT - distance) * VGA_WIDTH * 2;
    volatile unsigned short int *newBlankStart = scr
        + (VGA_HEIGHT - distance) * VGA_WIDTH;
    int blankBytesToSet = distance * VGA_WIDTH * 2;
    memcpy((unsigned char *)scr, newStart, bytesToCopy);
    memcpy2((unsigned char *)newBlankStart, (unsigned char *)&EmptySpace,
            2,blankBytesToSet);
    cury = VGA_HEIGHT - 1;

    return 0;
}

int putchar(unsigned char c){
    volatile unsigned short int *addr = 0;
    if (c == '\t') {
        curx = ((curx + 4)/4)*4;
    } else if (c == '\r') {
        curx = 0;
    } else if (c == '\n') {
        curx = 0;
        cury++;
    } else if (c == 0x08 && curx != 0) {
        curx--;
    } else {
        addr = scr + (cury * VGA_WIDTH + curx);
        *addr = COLOUR | c;
        curx++;
    }

    if (curx >= VGA_WIDTH) {
        curx = 0;
        cury++;
    }

    scroll();

    return 0;
}

int puts(unsigned char *str) {
    while (*str) {
        putchar(*str);
        str++;
    }
    return 0;
}

int vga_clear() {
    volatile char *vga = (volatile char*)VGA_ADDRESS;

    for (int i = 0 ; i < VGA_WIDTH * VGA_HEIGHT; i++) {
        vga[2*i] = ' ';
    }

    return 0;
}


int vga_init() {
    scr = (volatile unsigned short int *)VGA_ADDRESS;
    return vga_clear();
}

int snprintf(char *buffer, int length, char *format, ...) {
    char **arg = (char **) &format;
    int c;
    char buf[20];

    int index = 0;

    arg++;
    char c2;
    while ((c = *format++) != 0 && index < length) {
        if (c != '%') {
            if (index < length) {
                buffer[index++] = c;
            }
        } else {
            char *p;
            c = *format++;
            switch(c) {
                case 'd':
                case 'u':
                case 'x':
                    itoa(buf, c, *((int *) arg++));
                    p = buf;
                    goto string;
                    break;
                case 's':
                    p = *arg++;
                    if (p == 0) {
                        p = "(null)";
                    }
                string:
                    while(*p && index < length) {
                        buffer[index++] = *p++;
                    }
                    break;
                default:
                    c2 = (*((int *) arg++));
                    if (index < length) {
                        buffer[index++] = c2;
                    }
                    break;
            }
        }
    }

    return 0;
}

int printf(const char *format, ...) {
    char **arg = (char **) &format;
    int c;
    char buf[20];

    arg++;

    while ((c = *format++) != 0) {
        if (c != '%') {
            putchar(c);
        } else {
            char *p;
            c = *format++;
            switch(c) {
                case 'd':
                case 'u':
                case 'x':
                    itoa(buf, c, *((int *) arg++));
                    p = buf;
                    goto string;
                    break;
                case 's':
                    p = *arg++;
                    if (p == 0) {
                        p = "(null)";
                    }
                string:
                    while(*p) {
                        putchar(*p++);
                    }
                    break;
                default:
                    putchar(*((int *) arg++));
                    break;
            }
        }
    }

    return 0;
}

int log(const char *format, ...) {
    char **arg = (char **) &format;
    int c;
    char buf[20];

    arg++;

    while ((c = *format++) != 0) {
        if (c != '%') {
            write_serial(c);
        } else {
            char *p;
            c = *format++;
            switch(c) {
                case 'd':
                case 'u':
                case 'x':
                    itoa(buf, c, *((int *) arg++));
                    p = buf;
                    goto string;
                    break;
                case 's':
                    p = *arg++;
                    if (p == 0) {
                        p = "(null)";
                    }
                string:
                    while(*p) {
                        write_serial(*p++);
                    }
                    break;
                default:
                    write_serial(*((int *) arg++));
                    break;
            }
        }
    }

    return 0;
}
