#include <system.h>

multiboot_info_t *m;
u32int mem_lower;
u32int mem_upper;

fs_node_t *fs_root = NULL;

void pre_init(unsigned long magic) {
    vga_init();
    log("Zainicjalizowano ekran\r\n");
    init_serial();
    log("Zainicjalizowano port seryjny\r\n");

    if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        printf ("Invalid magic number: 0x%x\n", (unsigned) magic);
        PANIC("INVALID MULTIBOOT MAGIC");
        return;
    }

    if (CHECK_FLAG(m->flags, 0)) {
        mem_lower = m->mem_lower;
        mem_upper = m->mem_upper;
        printf("mem_lower: %uKB, mem_upper: %uKB\n",
                m->mem_lower, m->mem_upper);
    }

    if (CHECK_FLAG(m->flags, 1)) {
        printf("boot device: %s\n", (char *)m->boot_device);
    }

    if (CHECK_FLAG(m->flags, 2)) {
        printf("command line: %s\n", (char *)m->cmdline);
    }

    if (CHECK_FLAG(m->flags, 3)) {
        printf("nr of modules: %u\n", m->mods_count);
        multiboot_module_t *modules = (multiboot_module_t *)m->mods_addr;
        for (u32int i = 0; i < m->mods_count; i++) {
            printf("module nr %u: start: 0x%x, end: 0x%x, cmdline: %s\n",
                    i,
                    modules[i].mod_start,
                    modules[i].mod_end,
                    modules[i].cmdline);
        }
    }

    gdt_install();
    log("Zainstalowano struktury GDT\r\n");
    idt_install();
    log("Zainstalowano struktury IDT (przerwania)\r\n");
    isrs_install();
    log("Zainstalowano procedury obsługujące przerwania\r\n");

    initialise_paging2();

    irq_install();
    log("Zainstalowano przerwania irq\r\n");
    timer_install();
    log("Zainstalowano systemowy timer\r\n");
    kbd_install();
    log("Zainicjalizowano klawiature\r\n");
    sti();

    log("Wlaczono przerwania\r\n");
    log("Start: 0x%x\r\n", kernel_start());
    log("End: 0x%x\r\n", kernel_end());
    log("End (with stack): 0x%x\r\n", kernel_full_end());
    log("Size: 0x%x\r\n", kernel_end()-kernel_start());
    log("Size (with stack): 0x%x\r\n", kernel_full_end()-kernel_start());

    multiboot_module_t *modules = (multiboot_module_t *)m->mods_addr;

    u32int initrd_start = modules[0].mod_start;
    u32int initrd_end = modules[0].mod_end;

    printf("Initrd start: 0x%x\n", initrd_start);
    printf("Initrd end: 0x%x\n", initrd_end);


    for (u32int addr = initrd_start / 0x1000 * 0x1000; addr < initrd_end;
            addr += 0x1000) {
        printf ("Allocating addr: 0x%x\n", addr);
        ident_alloc_frame(get_page(addr, 1, kernel_directory),
                1, 1, addr);
    }

    fs_root = initrd_init(initrd_start);


}

void post_destroy() {
    init_acpi();
    AcpiEnable();
    AcpiPowerOff();

    for (;;) {
        hlt();
    }

}

int cmain(unsigned long magic, multiboot_info_t *mbi) {
    m = mbi;
    pre_init(magic);
    printf("Zaladowano system useless os\n");

    u32int k = 0;
    struct dirent *node = NULL;

    while ( (node = readdir_fs(fs_root, k)) != 0) {
        printf ("Found file: %s\n", node->name);
        fs_node_t *fsnode = finddir_fs(fs_root, node->name);
        if ((fsnode->flags & 0x7) == FS_DIRECTORY) {
            printf("\n\t(directory)\n");
        } else {
            printf("\n\t contents:\"");
            unsigned char buf[256];
            read_fs(fsnode, 0, 256, buf);
            printf("%s\"\n", buf);
            printf("\n");
        }
        k++;
    }

    for (u32int i = 0; i < 10; i++) {
        printf(".");
        sleep(1);
    }

    post_destroy();
    return 0;
}
