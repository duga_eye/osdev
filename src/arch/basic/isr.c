#include <isr.h>

extern int isr0();
extern int isr1();
extern int isr2();
extern int isr3();
extern int isr4();
extern int isr5();
extern int isr6();
extern int isr7();
extern int isr8();
extern int isr9();
extern int isr10();
extern int isr11();
extern int isr12();
extern int isr13();
extern int isr14();
extern int isr15();
extern int isr16();
extern int isr17();
extern int isr18();
extern int isr19();
extern int isr20();
extern int isr21();
extern int isr22();
extern int isr23();
extern int isr24();
extern int isr25();
extern int isr26();
extern int isr27();
extern int isr28();
extern int isr29();
extern int isr30();
extern int isr31();

int isrs_install() {
    idt_set_gate(0,(unsigned)isr0, 0x08, 0x8E);
    idt_set_gate(1,(unsigned)isr1, 0x08, 0x8E);
    idt_set_gate(2,(unsigned)isr2, 0x08, 0x8E);
    idt_set_gate(3,(unsigned)isr3, 0x08, 0x8E);
    idt_set_gate(4,(unsigned)isr4, 0x08, 0x8E);
    idt_set_gate(5,(unsigned)isr5, 0x08, 0x8E);
    idt_set_gate(6,(unsigned)isr6, 0x08, 0x8E);
    idt_set_gate(7,(unsigned)isr7, 0x08, 0x8E);
    idt_set_gate(8,(unsigned)isr8, 0x08, 0x8E);
    idt_set_gate(9,(unsigned)isr9, 0x08, 0x8E);
    idt_set_gate(10,(unsigned)isr10, 0x08, 0x8E);
    idt_set_gate(11,(unsigned)isr11, 0x08, 0x8E);
    idt_set_gate(12,(unsigned)isr12, 0x08, 0x8E);
    idt_set_gate(13,(unsigned)isr13, 0x08, 0x8E);
    idt_set_gate(14,(unsigned)isr14, 0x08, 0x8E);
    idt_set_gate(15,(unsigned)isr15, 0x08, 0x8E);
    idt_set_gate(16,(unsigned)isr16, 0x08, 0x8E);
    idt_set_gate(17,(unsigned)isr17, 0x08, 0x8E);
    idt_set_gate(18,(unsigned)isr18, 0x08, 0x8E);
    idt_set_gate(19,(unsigned)isr19, 0x08, 0x8E);
    idt_set_gate(20,(unsigned)isr20, 0x08, 0x8E);
    idt_set_gate(21,(unsigned)isr21, 0x08, 0x8E);
    idt_set_gate(22,(unsigned)isr22, 0x08, 0x8E);
    idt_set_gate(23,(unsigned)isr23, 0x08, 0x8E);
    idt_set_gate(24,(unsigned)isr24, 0x08, 0x8E);
    idt_set_gate(25,(unsigned)isr25, 0x08, 0x8E);
    idt_set_gate(26,(unsigned)isr26, 0x08, 0x8E);
    idt_set_gate(27,(unsigned)isr27, 0x08, 0x8E);
    idt_set_gate(28,(unsigned)isr28, 0x08, 0x8E);
    idt_set_gate(29,(unsigned)isr29, 0x08, 0x8E);
    idt_set_gate(30,(unsigned)isr30, 0x08, 0x8E);
    idt_set_gate(31,(unsigned)isr31, 0x08, 0x8E);
    return 0;
}

char *exception_messages[] = {
    "[0]: Division by zero",
    "[1]: Debug",
    "[2]: Non Maskable Interrupt",
    "[3]: Breakpoint",
    "[4]: Into Detected Overflow",
    "[5]: Out Of Bounds",
    "[6]: Invalid Opcode",
    "[7]: No Coprocessor Found",
    "[8]: Double Fault",
    "[9]: Coprocessor Segment Overrun",
    "[10]: Bad TSS",
    "[11]: Segment Not Present",
    "[12]: Stack Fault",
    "[13]: General Protection Fault",
    "[14]: Page Fault",
    "[15]: Unknown Interrupt",
    "[16]: Coprocessor Fault",
    "[17]: Alignment Check",
    "[18]: Machine Check",
    "[19]: Reserved",
    "[20]: Reserved",
    "[21]: Reserved",
    "[22]: Reserved",
    "[23]: Reserved",
    "[24]: Reserved",
    "[25]: Reserved",
    "[26]: Reserved",
    "[27]: Reserved",
    "[28]: Reserved",
    "[29]: Reserved",
    "[30]: Reserved",
    "[31]: Reserved",
};

int fault_handler(struct regs *r) {
//    printf("AAA");
    /* Is this a fault whose number is from 0 to 31? */
    if (r->int_no < 32)
    {
        /* Display the description for the Exception that occurred.
        *  In this tutorial, we will simply halt the system using an
        *  infinite loop */
        log(exception_messages[r->int_no]);
        log(" Exception. System Halted!\n");
        printf(exception_messages[r->int_no]);
        printf(" Exception. System Halted!\n");

        for (;;);
    }
    return 0;
}
