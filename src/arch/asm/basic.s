section .text

extern gp
extern idtp

global outb
global outw
global inb
global inw
global gdt_flush
global idt_load
global sti
global cli
global io_wait
global disablePic
global remap

global kernel_start
global kernel_end
global kernel_full_end
global crash
global hlt
global disablePaging
global shutdown

extern _start
extern stack
extern stack_end

outb:
    push ebp
    mov ebp, esp

    push edx
    push eax

    mov dx, [ebp+8]  ; port
    mov al, [ebp+12] ; dane ; TODO gcc zwyczajnie unsigned short zaokrągla do 4
                            ; dlaczego to robi, nie wiem ???
    out dx, al

    pop eax
    pop edx

    pop ebp
    ret

outw:
    push ebp
    mov ebp, esp

    push edx
    push eax

    mov dx, [ebp+8]
    mov ax, [ebp+12]

    out dx, ax

    pop eax
    pop edx

    pop ebp
    ret

inb:
    push ebp
    mov ebp, esp

    push edx

    mov dx, [ebp+8] ; port
    xor eax,eax

    in al, dx

    pop edx

    pop ebp
    ret

inw:
    push ebp
    mov ebp, esp

    push edx

    mov dx, [ebp+8] ; port
    xor eax,eax

    in ax, dx

    pop edx

    pop ebp
    ret


gdt_flush:
    lgdt [gp]
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp 0x08:gdt_flush2

gdt_flush2:
    ret

idt_load:
    lidt [idtp]
    ret

sti:
    sti
    ret

cli:
    cli
    ret

io_wait:
    jmp wait1
wait1:
    jmp wait2
wait2:
    ret

disablePic:
    mov al, 0xff
    out 0xa1, al
    out 0x21, al
    ret

remap:
    mov al, 0x11
    out 0x20, al     ;restart PIC1
    out 0xA0, al     ;restart PIC2

    mov al, 0x20
    out 0x21, al     ;PIC1 now starts at 32
    mov al, 0x28
    out 0xA1, al     ;PIC2 now starts at 40

    mov al, 0x04
    out 0x21, al     ;setup cascading
    mov al, 0x02
    out 0xA1, al

    mov al, 0x01
    out 0x21, al
    out 0xA1, al     ;done!
    ret

kernel_start:
    mov eax, _start
    ret
kernel_end:
    mov eax, stack
    ret
kernel_full_end:
    mov eax, stack_end
    ret

crash:
    int 14
    ret

hlt:
    hlt
    ret

disablePaging:
    push ebp
    mov ebp, esp

    push eax
    push ebx

    mov eax, cr0
    mov ebx, 0x80000000
    not ebx
    and eax, ebx
    mov cr0, eax

    pop ebx
    pop eax

    pop ebp

    ret

shutdown:
    mov al, 0x20
    mov dx, 0xb004
    out dx, al
    xor eax, eax
    out dx, al
    ret
