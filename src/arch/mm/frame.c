#include <frame.h>

u32int *frames;
u32int nframes;

void set_frame(u32int frame_addr) {
    u32int frame = frame_addr / 0x1000;
    u32int idx = INDEX_FROM_BIT(frame);
    u32int off = OFFSET_FROM_BIT(frame);
    frames[idx] |= (0x1 << off);
    // log("Frame 0x%x used\r\n", frame_addr);
}

void clear_frame(u32int frame_addr) {
    u32int frame = frame_addr / 0x1000;
    u32int idx = INDEX_FROM_BIT(frame);
    u32int off = OFFSET_FROM_BIT(frame);
    frames[idx] &= ~(0x1 << off);
    // log("Frame 0x%x freed\r\n", frame_addr);
}

u32int test_frame(u32int frame_addr) {
    u32int frame = frame_addr / 0x1000;
    u32int idx = INDEX_FROM_BIT(frame);
    u32int off = OFFSET_FROM_BIT(frame);
    return (frames[idx] & (0x1 << off));
}

u32int first_frame() {
    u32int i,j;
    for (i = 0; i < INDEX_FROM_BIT(nframes); i++) {
        if (frames[i] != 0xFFFFFFFF) {
            for (j = 0; j < 32; j++) {
                u32int toTest = 0x1 << j;
                if (!(frames[i]&toTest)) {
                    return i*4*8+j;
                }
            }
        }
    }
    return -1;
}

u32int frames_allocated() {
    u32int count = 0;

    for (u32int frameNr = 0; frameNr < nframes; frameNr++) {
        if (test_frame(frameNr * 0x1000)) {
//            log("Frame allocated: 0x%x\r\n", frameNr * 0x1000);
            count++;
        }
    }

    return count;
}

void dump_frames() {
    u32int start,end;
    u32int block = 0;
    u32int frameNr = 0;
    for (frameNr = 0; frameNr < nframes; frameNr++) {
        if (test_frame(frameNr * 0x1000)) {
            if (!block) {
                block = 1;
                start = frameNr;
            }
        } else {
            if (block) {
                block = 0;
                end = frameNr;

                log("[0x%x - 0x%x] [0x%x(%d)]\r\n",
                        start,
                        end-1,
                        end-start,
                        end-start);

                printf("[0x%x - 0x%x] [0x%x(%d)]\n",
                        start,
                        end-1,
                        end-start,
                        end-start);


            }
        }
    }

}

void alloc_frame(page_t *page, int is_kernel, int is_writable) {
    /*
    log("Alloc frame 0x%x [%c%c]\r\n",
            page->frame * 0x1000,
            page->user ? 'u' : 'k',
            page->rw ? 'w' : 'r');
    */

    if (page->frame != NULL) {
        //log("Strona juz zalokowana: 0x%x\r\n",  page->frame*0x1000);
        return;
    } else {
        u32int idx = first_frame();
        if (idx == (u32int)-1) {
            PANIC("No free frames");
            return;
        }

        set_frame(idx*0x1000);
        page->present = 1;
        page->rw = (is_writable) ? 1 : 0;
        page->user = (is_kernel) ? 0 : 1;
        page->frame = idx;
        // log("Strona juz zaalokowano: 0x%x\r\n",  page->frame*0x1000);
    }
}

void ident_alloc_frame(page_t *page, int is_kernel, int is_writable,
        u32int addr) {
    /*
    printf("ident alloc frame: 0x%x\n", page);
    log("Alloc frame 0x%x [%c%c]\r\n",
            page->frame * 0x1000,
            page->user ? 'u' : 'k',
            page->rw ? 'w' : 'r');
    */
    if (page->frame != NULL) {
        //log("Strona juz zalokowana: 0x%x\r\n",  page->frame*0x1000);
        return;
    } else {
        u32int idx = addr / 0x1000;
        if (idx == (u32int)-1) {
            PANIC("No free frames");
            return;
        }

        set_frame(idx*0x1000);
        page->present = 1;
        page->rw = (is_writable) ? 1 : 0;
        page->user = (is_kernel) ? 0 : 1;
        page->frame = idx;
      //  log("Strona juz zaalokowano: 0x%x\r\n",  page->frame*0x1000);
    }
}


void free_frame(page_t *page) {
    // log("free frame: 0x%x\r\n", page->frame * 0x1000);
    u32int frame;
    if (!(frame = page->frame)) {
        // log("frame was not assigned\r\n");
        return; /** ponieważ dana strona nie ma przypisanej ramki pamięci */
    } else {
        clear_frame(frame*0x1000);
        page->frame = 0x0;
       // log("cleared frame\r\n");
    }
}
