#include <task/copy.h>

page_directory_t *clone_directory(page_directory_t *src) {
    page_directory_t *dir = (page_directory_t *)kmalloc_a(
            sizeof(page_directory_t));
    u32int phys = c_log_phys((u32int)dir);
    memset(dir, 0, sizeof(page_directory_t));

    u32int offset = (u32int)dir->tablesPhysical - (u32int)dir;
    dir->physicalAddr = phys + offset;

    for (u32int i = 0; i < 1024; i++) {
        if (!src->table[i]) {
            continue;
        }

        if (kernel_directory->table[i] == src->table[i]) {
            // takie same są już w jądrze, linkujemy
            dir->table[i] = src->table[i];
            dir->tablesPhysical[i] = src->tablesPhysical[i];
        } else {
            u32int phys;
            dir->table[i] = clone_table(src->table[i], &phys);
            dir->tablesPhysical[i] = phys | 0x07;
        }
    }

    return dir;
}


page_table_t *clone_table(page_table_t *src, u32int *phys) {
    page_table_t *table = (page_table_t *)kmalloc_a(sizeof(page_table_t));
    *phys = c_log_phys((u32int)table);
    memset(table, 0, sizeof(page_directory_t));

    for(u32int i = 0; i < 1024; i++) {
        if (!src->pages[i].frame) {
            continue;
        }

        alloc_frame(&table->pages[i], 0, 0);

        if (src->pages[i].present) {
            table->pages[i].present = 1;
        }

        if (src->pages[i].rw) {
            table->pages[i].rw = 1;
        }

        if (src->pages[i].user) {
            table->pages[i].user = 1;
        }

        if (src->pages[i].accessed) {
            table->pages[i].accessed = 1;
        }

        if (src->pages[i].dirty) {
            table->pages[i].dirty = 1;
        }

        copy_page_physical(src->pages[i].frame*0x1000,
                table->pages[i].frame*0x1000);

    }

    return table;
}
