#include <acpi/acpi.h>
#include <acpi/fadt.h>
#include <pit.h>

char RDSP_magic[] = "RSD PTR ";

RSDPv1_t *rdsp;
SDT_header_t *facp ;
struct FADT *fadt;

u32int find_RDSP() {
    for (u32int addr = 0x000E0000; addr <= 0x000FFFFF; addr++) {
        if (!memcmp((void *)addr, RDSP_magic, 8) == 0) {
            continue;
        }

        if (rsdp_verification((RSDPv1_t *)addr)) {
            return addr;
        }

    }

    return 0xFFFFFFFF;
}

u8int SLP_TYPa = 0xFF;
u8int SLP_TYPb = 0xFF;

void init_acpi () {
    u32int rdsp_location = find_RDSP();
    if (rdsp_location == 0xFFFFFFFF) {
        printf("No ACPI detected\n");
        return;
    }

    rdsp = (RSDPv1_t *)rdsp_location;
    printf("rsdp location: 0x%x\n", rdsp);
    printf("rsdp revision: 0x%x\n", rdsp->revision);
    u32int facp_location = find_header(rdsp->rsdt_address,
            "FACP");

    u32int ssdt_location = find_header(rdsp->rsdt_address,
            "SSDT");

    printf("FACP location: 0x%x\n", facp_location);
    printf("SSDT location: 0x%x\n", ssdt_location);
    SDT_header_t *ssdt = (SDT_header_t *)ssdt_location;
    if (!header_verification(ssdt)) {
        printf("SSDT failed\n");
        return;
    }
    
    char *data = (char *)(ssdt_location + sizeof(SDT_header_t));
    //dump((void *)data, 256);

    char *s5 = NULL;

    for (u32int i = 0; i < ssdt->Length - sizeof(SDT_header_t); i++) {
        if (memcmp(&data[i], "_S5_", 4) == 0) {
            printf ("IDX: %d\n", i);
            s5 = (char *)&data[i];
            break;
        }
    }

    //dump((void *)(((u32int)s5/16)*16), 256);

    SLP_TYPa = 0xFF;
    SLP_TYPb = 0xFF;
    
     if ((*(s5-1) == 0x08 || (*(s5-2) == 0x08 && *(s5-1) == '\\')) && *(s5+4) == 0x12) {
         printf("Valid S5\n");

        // advance & compute PkgLength size
        s5 += 5;
        s5 += ((*s5 & 0xC0)>>6) + 2;

        // skip byteprefix
        if (*s5 == 0x0A)
            s5++;

        printf("Hex value1: 0x%x, location: 0x%x\n", *(s5), s5);
        SLP_TYPa = *(s5)<<10;

        s5++;
        // skip byteprefix
        if (*s5 == 0x0A)
            s5++;

        printf("Hex value2: 0x%x, location: 0x%x\n", *(s5), s5);
        SLP_TYPb = *(s5)<<10;

        
    } else {
        printf("No valid S5\n");
        return;
    }

    printf ("SLP_TYPa: 0x%x\n", SLP_TYPa);
    printf ("SLP_TYPb: 0x%x\n", SLP_TYPb);


    fadt= (struct FADT *)facp_location;

    printf ("SMI_CommandPort: 0x%x\n", fadt->SMI_CommandPort);
    printf ("AcpiEnable: 0x%x, AcpiDisable: 0x%x\n",
            fadt->AcpiEnable, fadt->AcpiDisable);

}

void AcpiEnable() {
    sti();
    outb(fadt->SMI_CommandPort, fadt->AcpiEnable);
    printf ("Waiting to confirm status ON of ACPI\n");
    for (u32int i = 0; i < 10; i++) {
        sleep(1.0);
        printf(".");
        if(inb(fadt->PM1aControlBlock) & 1) {
            printf("\nEnabled\n");
            break;
        }
    }
    printf("\nWaiting finished\n");
}


void AcpiPowerOff() {
    sti();
    u32int SLP_EN = 1 << 13;
    u32int command1 = SLP_TYPa | SLP_EN;
    u32int command2 = SLP_TYPb | SLP_EN;

    printf ("Command1: 0x%x, Command2: 0x%x\n",
            command1, command2);

    printf("Fadt: 0x%x\n", fadt->PM1aControlBlock);

    if (fadt->PM1aControlBlock) {
        printf("Executing command 1\n");
        outw(fadt->PM1aControlBlock, command1);
    } else {
        printf ("No possible command 1\n");
    }

    if (fadt->PM1bControlBlock) {
        printf("Executing command 2\n");
        outw(fadt->PM1bControlBlock, command2);
    } else {
        printf ("No possible command 2\n");
    }

    printf("ACPI Power off failed\n");
    printf("Waiting 5 seconds\n");
    for (u32int i = 0; i < 5; i++) {
        printf (".");
        sleep(1);
    }
    printf(" Finished\n");
    for(;;) {
        hlt();
    }


}

u32int rsdp_verification(RSDPv1_t *rdsp) {
    u8int *addr = (u8int *)(((u32int)rdsp));
    u32int size = sizeof(RSDPv1_t);
    u8int calculated = 0;

    printf("Rozmiar RSDP: %d\n", sizeof(RSDPv1_t));
    u32int checksum_location = (u32int)(&rdsp->checksum);
    u32int offset = (checksum_location-(u32int)rdsp);
    printf ("Checksum offset: %d\n", offset);

    for (u32int i = 0; i < size; i++) {
        printf("0x%x ", addr[i]);
        calculated += addr[i];
    }
    printf("\n");

    printf("Calculated checksum: 0x%x\n", calculated);

    if (calculated == 0) {
        printf("checksum valid\n");
        return 1;
    } else {
        printf("checksum not valid\n");
        return 0;
    }
}

u32int header_verification(SDT_header_t *header) {
    printf("header verificaton\n");
    u8int *addr = (u8int *)header;
    u8int calculated = 0;
    ident_alloc_frame(get_page((u32int)addr, 1, kernel_directory),1, 1, 
            (u32int)addr);
    printf("header verification 0x%x -> length: 0x%x\n",
            header, header->Length);
    printf("Struct loc offset: 0x%x\n",
            (u32int)&(header->Length) - (u32int)header);
    printf("Dumping header of SDT\n");
    //dump(addr, ((sizeof(SDT_header_t)-1) / 16 + 1)*16);

    for (u32int i = 0; i < header->Length; i++) {
        u8int *loc = &addr[i];
        if ((u32int)loc % 0x1000 == 0) {
            printf("Allocating at: 0x%x\n", (u32int)loc);
            ident_alloc_frame(get_page((u32int)loc, 1, kernel_directory),1, 1,
                    (u32int)loc);
        }
        calculated += addr[i];
    }

    if (calculated == 0) {
        printf("Header verified\n");
        return 1;
    } else {
        printf("Header bad\n");
        return 0;
    }
}

u32int find_header(u32int rsdt, char signature[4]) {

    printf("RSDT location: 0x%x\n", rsdt);
    ident_alloc_frame(get_page(rsdt, 1, kernel_directory),1, 1,
            (u32int)rsdt);
    SDT_header_t *h = (SDT_header_t *)rsdt;
    printf("OK\n");
    printf("RSDT signature: %s\n", h->Signature);

    if (!header_verification(h)){
        printf("RDST not valid\n");
        return 0xFFFFFFFF;
    } else {
        printf("RSDT Header valid\n");
    }

    printf("Finding...\n");

    printf("header length: %d\n", h->Length);
    if (h->Length < sizeof(SDT_header_t)) {
        printf("Too small header... weird\n");
        return 0xFFFFFFFF;
    }

    u32int entry_count = (h->Length - sizeof(SDT_header_t)) / 4;
    printf("Entry count: %d\n", entry_count);
    u32int *entries = (u32int *)(rsdt + sizeof(SDT_header_t));

    printf("Entries: 0x%x\r\n", entries);

    for (u32int i = 0; i < entry_count; i++) {
        SDT_header_t *header = (SDT_header_t *)entries[i];
        printf("Header nr %d location: 0x%x\r\n", i, header);
        printf("Header signature: %s\n", header->Signature);
        ident_alloc_frame(get_page((u32int)header, 1, kernel_directory),1, 1,
                (u32int)header);
        if (memcmp(header->Signature, signature, 4) == 0) {
            printf("Found signature: %s\n", header->Signature);
            if (header_verification(header)) {
                printf("Header verified\n");
                return entries[i];
            }
        }
    }

    return 0xFFFFFFFF;

}
