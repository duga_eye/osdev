#include <fs/initrd.h>

int main(int argc, char **argv) {

    if (argc < 2) {
        fprintf(stderr, "No input files\n");
        return 1;
    }

    fprintf (stderr, "Rozmiar struktury: %x\n", sizeof(initrd_header_t));

    for (u32int i = 1; i < argc; i++) {
        fprintf(stderr, "File nr %d to embed: '%s'\n",
                i, argv[i]);
    }

    u32int nrFiles = 0;

    FILE *f = fopen("initrd.img", "w+");
    if (!f) {
        fprintf(stderr, "I cannot open file 'initrd.img for write\n");
        return 2;
    }

    u32int offset = 0;
    u32int buffer[12];
    memset(buffer, 0, sizeof(buffer));
    fwrite(&nrFiles, 4, 1, f); // teraz tylko dla wyrównania danych
    fwrite(buffer, 1, 12, f);  // wyrównanie do 16

    initrd_header_t *headers = (initrd_header_t *)
        malloc(sizeof(initrd_header_t) * (argc - 1));
    memset(headers, 0, sizeof(initrd_header_t) * (argc - 1));

    fprintf(stderr, "Filling in headers\n");

    for (u32int i = 1; i < argc; i++) {
        fprintf (stderr, "writing file %s to initrd.img\n",
                argv[i]);

        FILE *fAttachment = fopen(argv[i], "r");
        if (!fAttachment) {
            fprintf (stderr, "cannot open attachment %s to read\n",
                    argv[i]);
            continue;
        }

        if (fseek(fAttachment, 0, SEEK_END) < 0) {
            fprintf (stderr, "cannot rewind attachment %s to the end\n",
                    argv[i]);
            continue;
        }

        long size = ftell(fAttachment);
        if (size < 0) {
            fprintf (stderr, "cannot acquire current position in file %s\n",
                    argv[i]);
            continue;
        }

        fprintf (stderr, "Rozmiar pliku: 0x%x\n", size);

        if (fseek(fAttachment, 0, SEEK_SET) < 0) {
            fprintf (stderr, "cannot rewind attachment %s to the start\n",
                    argv[i]);
            continue;
        }

        fclose(fAttachment); // zamykamy, na co nam jest jeszcze potrzebny

        headers[nrFiles].magic = INITRD_MAGIC;
        char *bName = basename(argv[i]);
        fprintf (stderr, "Base name: %s\n", bName);
        strncpy(headers[nrFiles].name ,bName, INITRD_NAME_LENGTH);
        headers[nrFiles].length = size;
        headers[nrFiles].offset = offset;
        offset += size;

        fprintf (stderr, "[%d] header MAGIC: 0x%x\n", nrFiles,
                headers[nrFiles].magic);
        fprintf (stderr, "[%d] header name: '%s'\n", nrFiles,
                headers[nrFiles].name);
        fprintf (stderr, "[%d] header offset: 0x%x\n", nrFiles,
                headers[nrFiles].offset);
        fprintf (stderr, "[%d] header length: 0x%x\n", nrFiles,
                headers[nrFiles].length);

        nrFiles++;
        // zwiększamy liczbę plików, które zapisaliśmy faktycznie
        // do initrd.img

    }

    fwrite(headers, 1, sizeof(initrd_header_t) * nrFiles, f);

    // zapisane nagłówki initrd.

    fprintf(stderr, "Filling in file content\n");

    u32int idx = 0;

    for (u32int i = 1; i < argc; i++) {
        fprintf (stderr, "writing file %s to initrd.img\n",
                argv[i]);

        FILE *fAttachment = fopen(argv[i], "r");
        if (!fAttachment) {
            fprintf (stderr, "cannot open attachment %s to read\n",
                    argv[i]);
            continue;
        }

        char *content = (char *)malloc(sizeof(char) * headers[idx].length);
        if (!content) {
            fprintf (stderr, "cannot allocate buffer for file content\n");
            continue;
        }
        int bytesRead = fread(content, 1, headers[idx].length, fAttachment);
        if (bytesRead < 0) {
            fprintf (stderr, "cannot read open attachment file: %s\n",
                    argv[i]);
            continue;
        }

        fclose(fAttachment); // zamykamy, na co nam jest jeszcze potrzebny

        fwrite(content, 1, headers[idx].length, f);
        free(content);
        idx++;
    }

    fseek(f, 0, SEEK_SET);

    fwrite(&nrFiles, 4, 1, f); // nadpisujemy prawidłową liczbę plików

    fclose(f);
    free(headers);
    return 0;
}
