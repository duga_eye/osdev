#include <system.h>

multiboot_info_t *m;
u32int mem_lower;
u32int mem_upper;

void pre_init(unsigned long magic) {
    vga_init();
    log("Zainicjalizowano ekran\r\n");
    init_serial();
    log("Zainicjalizowano port seryjny\r\n");

    if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        printf ("Invalid magic number: 0x%x\n", (unsigned) magic);
        PANIC("INVALID MULTIBOOT MAGIC");
        return;
    }

    if (CHECK_FLAG(m->flags, 0)) {
        mem_lower = m->mem_lower;
        mem_upper = m->mem_upper;
        printf("mem_lower: %uKB, mem_upper: %uKB\n",
                m->mem_lower, m->mem_upper);
    }

    if (CHECK_FLAG(m->flags, 1)) {
        printf("boot device: %s\n", (char *)m->boot_device);
    }

    if (CHECK_FLAG(m->flags, 2)) {
        printf("command line: %s\n", (char *)m->cmdline);
    }

    if (CHECK_FLAG(m->flags, 3)) {
        printf("nr of modules: %u\n", m->mods_count);
        multiboot_module_t *modules = (multiboot_module_t *)m->mods_addr;
        for (u32int i = 0; i < m->mods_count; i++) {
            printf("module nr %u: start: 0x%x, end: 0x%x, cmdline: %s\n",
                    i,
                    modules[i].mod_start,
                    modules[i].mod_end,
                    modules[i].cmdline);
        }
    }

    gdt_install();
    log("Zainstalowano struktury GDT\r\n");
    idt_install();
    log("Zainstalowano struktury IDT (przerwania)\r\n");
    isrs_install();
    log("Zainstalowano procedury obsługujące przerwania\r\n");

    initialise_paging2();

    irq_install();
    log("Zainstalowano przerwania irq\r\n");
    timer_install();
    log("Zainstalowano systemowy timer\r\n");
    kbd_install();
    log("Zainicjalizowano klawiature\r\n");
    sti();

    log("Wlaczono przerwania\r\n");
    log("Start: 0x%x\r\n", kernel_start());
    log("End: 0x%x\r\n", kernel_end());
    log("End (with stack): 0x%x\r\n", kernel_full_end());
    log("Size: 0x%x\r\n", kernel_end()-kernel_start());
    log("Size (with stack): 0x%x\r\n", kernel_full_end()-kernel_start());


}

void post_destroy() {
    cli();
    disablePaging();
    sti();
    init_acpi();
    AcpiEnable();
    AcpiPowerOff();

    for (;;) {
        hlt();
    }

}

int cmain(unsigned long magic, multiboot_info_t *mbi) {
    m = mbi;
    pre_init(magic);
    printf("Zaladowano system useless os\n");

    for (u32int i = 0; i < 10; i++) {
        printf(".");
        sleep(1);
    }

    post_destroy();
    return 0;
}
