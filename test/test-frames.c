#include <system.h>

multiboot_info_t *m;
u32int mem_lower;
u32int mem_upper;

void pre_init(unsigned long magic) {
    vga_init();
    log("Zainicjalizowano ekran\r\n");
    init_serial();
    log("Zainicjalizowano port seryjny\r\n");

    if (magic != MULTIBOOT_BOOTLOADER_MAGIC) {
        printf ("Invalid magic number: 0x%x\n", (unsigned) magic);
        PANIC("INVALID MULTIBOOT MAGIC");
        return;
    }

    if (CHECK_FLAG(m->flags, 0)) {
        mem_lower = m->mem_lower;
        mem_upper = m->mem_upper;
        printf("mem_lower: %uKB, mem_upper: %uKB\n",
                m->mem_lower, m->mem_upper);
    }

    if (CHECK_FLAG(m->flags, 1)) {
        printf("boot device: %s\n", (char *)m->boot_device);
    }

    if (CHECK_FLAG(m->flags, 2)) {
        printf("command line: %s\n", (char *)m->cmdline);
    }

    if (CHECK_FLAG(m->flags, 3)) {
        printf("nr of modules: %u\n", m->mods_count);
        multiboot_module_t *modules = (multiboot_module_t *)m->mods_addr;
        for (u32int i = 0; i < m->mods_count; i++) {
            printf("module nr %u: start: 0x%x, end: 0x%x, cmdline: %s\n",
                    i,
                    modules[i].mod_start,
                    modules[i].mod_end,
                    modules[i].cmdline);
        }
    }

    gdt_install();
    log("Zainstalowano struktury GDT\r\n");
    idt_install();
    log("Zainstalowano struktury IDT (przerwania)\r\n");
    isrs_install();
    log("Zainstalowano procedury obsługujące przerwania\r\n");

    initialise_paging2();

    irq_install();
    log("Zainstalowano przerwania irq\r\n");
    timer_install();
    log("Zainstalowano systemowy timer\r\n");
    kbd_install();
    log("Zainicjalizowano klawiature\r\n");
    sti();

    log("Wlaczono przerwania\r\n");
    log("Start: 0x%x\r\n", kernel_start());
    log("End: 0x%x\r\n", kernel_end());
    log("End (with stack): 0x%x\r\n", kernel_full_end());
    log("Size: 0x%x\r\n", kernel_end()-kernel_start());
    log("Size (with stack): 0x%x\r\n", kernel_full_end()-kernel_start());


}

void post_destroy() {
    cli();
    disablePaging();
    sti();
    init_acpi();
    AcpiEnable();
    AcpiPowerOff();

    for (;;) {
        hlt();
    }

}

u32int tests_count = 0;
u32int tests_succeeded_count = 0;

void test_ident_allocation() {
    log("Test %s started\n", __FUNCTION__);
    // na początku, przy uruchomieniu
    ++tests_count;

    dump_frames();
    u32int allocated1 = frames_allocated();
    log("Frames allocated number: 0x%x\n", allocated1);

    u32int addr = 0x200 * 0x1000;
    ident_alloc_frame(get_page(addr, 1, kernel_directory),
            1,
            1,
            addr);

    if (!test_frame(addr)) {
        log("test ident failed. Requested addr is not 1:1 mapped\r\n");
        return;
    }

    dump_frames();
    u32int allocated2 = frames_allocated();
    log("Frames allocated number: 0x%x\n", allocated2);

    if (allocated2 != allocated1 + 1) {
        log("Number of frames allocated has not grown by 1\r\n");
        return;
    }

    free_frame(get_page(addr, 0, kernel_directory));

    dump_frames();
    u32int allocated3 = frames_allocated();

    if (allocated1 != allocated3) {
        log("Number of frames has not lowered by 1, not equal before\r\n");
        return;
    }

    // na końcu, po uruchomieniu
    ++tests_succeeded_count;
    log("Test %s ended\n", __FUNCTION__);
}

void test_allocation() {
    log("Test %s started\n", __FUNCTION__);
    // na początku, przy uruchomieniu
    ++tests_count;

    dump_frames();
    u32int allocated1 = frames_allocated();
    log("Frames allocated number: 0x%x\n", allocated1);

    u32int addr = 0x200 * 0x1000;
    page_t *page = get_page(addr, 1, kernel_directory);

    alloc_frame(page, 1, 1);

    if (!test_frame(page->frame * 0x1000)) {
        log("test allocation failed. Requested addr is not mapped\r\n");
        return;
    }

    dump_frames();
    u32int allocated2 = frames_allocated();
    log("Frames allocated number: 0x%x\n", allocated2);

    if (allocated2 != allocated1 + 1) {
        log("Number of frames allocated has not grown by 1\r\n");
        return;
    }

    free_frame(get_page(addr, 0, kernel_directory));

    dump_frames();
    u32int allocated3 = frames_allocated();

    if (allocated1 != allocated3) {
        log("Number of frames has not lowered by 1, not equal before\r\n");
        return;
    }

    // na końcu, po uruchomieniu
    ++tests_succeeded_count;
    log("Test %s ended\n", __FUNCTION__);
}


void run_tests() {
    test_ident_allocation();
    test_allocation();

    if (tests_count == tests_succeeded_count){
        log("ALL TESTS PASSED\r\n");
    } else {
        log("SOME TESTS FAILED\r\n");
    }
}


int cmain(unsigned long magic, multiboot_info_t *mbi) {
    m = mbi;
    pre_init(magic);
    printf("Zaladowano system useless os\n");

    run_tests();

    post_destroy();
    return 0;
}
